# Maintainer: omicronns omicronns_at_gmail_dot_com

pkgname=radare2-cutter-git
_gitcommit=25d2606bfc4f3d31bbbbccbb61931e8d91ac4505
pkgver=1.2
pkgrel=1
pkgdesc='Qt and C++ GUI for radare2 reverse engineering framework (originally named Iaito)'
url='https://github.com/radareorg/cutter'
arch=('x86_64')
license=('GPL3')
depends=('radare2' 'capstone' 'qt5-base' 'qt5-svg' 'icu')
makedepends=('git' 'cmake')
provides=('radare2-cutter')
source=(${pkgname}::"git+https://github.com/radareorg/cutter#commit=${_gitcommit}")
sha512sums=('SKIP')

pkgver() {
  cd ${pkgname}
  # Remove 'v' prefix on tags; prefix revision with 'r'; replace all '-' with '.'
  git describe --always --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
  mkdir -p "${pkgname}/build"
  cd "${pkgname}/build"

  cmake -DCMAKE_BUILD_TYPE=Release ../src
}

build() {
  cd ${pkgname}/build
  make
}

package() {
  cd ${pkgname}

  install -DTm 755 build/cutter "${pkgdir}/usr/bin/cutter"
  install -DTm 644 src/cutter.desktop "${pkgdir}/usr/share/applications/cutter.desktop"
  install -DTm 644 src/img/cutter.svg "${pkgdir}/usr/share/icons/hicolor/scalable/apps/cutter.svg"
  install -DTm 644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  install -dm 755 docs/ "${pkgdir}/usr/share/doc/${pkgname}/"

  cp -a docs/* "${pkgdir}/usr/share/doc/${pkgname}/"
}
